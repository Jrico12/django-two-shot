from django.urls import include, path
from django.contrib import admin
from . import views

app_name = 'receipts'

urlpatterns = [
    path('', views.receipt_list, name='home'),
    path('admin/', admin.site.urls),
    path('receipts/', include('receipts.urls')),
]
