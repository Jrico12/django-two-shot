from django.shortcuts import render
from .models import Receipt
from django.shortcuts import redirect
# Create your views here.
def receipt_list(request):
    receipts = Receipt.objects.all()
    context = {
        'receipts': receipts,
    }
    return render(request, 'receipts/receipt_list.html', context)

